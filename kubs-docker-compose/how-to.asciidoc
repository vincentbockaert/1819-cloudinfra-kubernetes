= Describes which steps need to be take and in what order
== generate secret for the mysql-connection
=== this secret will be used by the spring boot application to establish a connection to the mysql container

[source, bash]
---
kubectl create secret generic mysql --from-literal=password=kubern3tesisNietEZ!
---

== deploy mysql (mysql-container, mysql-service, mysql-configMaps, mysql-volumeClaim)

[source, bash]
---
kubectl create -f mysql.yaml # DONT worry if it takes a while to go from red to green, this has to with binding the volume which takes a while
---

== deploy spring boot (container, service and configMap)

[source, bash]
---
kubectl create -f springboot.yaml
---

== deploy NGINX (container and loadbalancer-service)

[source, bash]
---
kubectl create -f nginx.yaml # it could take a while for the loadbalancer to assign the public ip
---

== visit the application by surfing to https://PUBLIC_IP/users
