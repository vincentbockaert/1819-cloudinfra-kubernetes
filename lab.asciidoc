= LABO Kubernetes

== Requirements

Om deze opdracht tot een goed einde te brengen, is toegang tot een Kubernetes cluster noodzakelijk.
Je kan daarvoor gebruik maken van de demo-cluster die werd opgezet, of van een publieke cloud provider.
Omdat het resultaat universeel is, kan je op éénder welk moment vlot overschakelen tussen beide.


== Voorbereiding

Voor deze opgave gaan jullie (opgave mag per team gemaakt worden) een kubernetes-infrstructuur bouwen voor een applicatie die minstens uit drie delen bestaat. (="3-tier")

Voorbeeld:
- Dat kan bijvoorbeeld een toepassing zijn met een losstaande API en een databank
- een toepassing met een databank én een syslog-server
- een remote bereikbare databank mét een grafana instantie voor visualisatie
- een applicatie met een databank én een caching container
- een applicatie met een https proxy en een databank
- ...

Er zijn honderden voorbeelden te bedenken, dus elk team wordt verondersteld een ander project te hebben. 
Eventueel kan je een eigen applicatie (vb uit het eerste of tweede jaar) een tweede leven geven in een container-cluster.

Maak een schets van de applicatie die je wil bewaren in containers. +
Maak zeker de namen van de verschillende componenten goed zichtbaar, en noteer +
poorten waar die bekend zijn. 

Maak een repository aan, en deel deze met de docenten, waarin alle yaml-files verzameld worden.
Zorg ervoor dat alle informatie in de repository aanwezig is om in het geval dat een cluster onbereikbaar wordt, meteen kan opnieuw opgebouwd worden op een andere cluster.


WARNING: Leg deze voor aan de docent voor je start met de implementatie.


== Leerdoelen

* Concept container management begrijpen
* De verschillende onderdelen van een Kubernetes cluster kunnen beheren
* Gebruik van kubectl


== Opgave

TIP: maak deze gehele opgave in één specifieke kubernetes-namespace.

* Maak en/of test de containers die je wenst te gebruiken.
* bepaal welke informatie in 'secrets' moet komen en maak deze aan.
* bepaal welke informatie persistent moet zijn.

* Maak een minimum viable product: net voldoende om de werking aan te tonen
* werk de opgave verder uit zodat:
** minstens één van de onderdelen vlot kan opgeschaald worden met meerdere instances
** Er efficiënt gebruik gemaakt wordt van resources (vb compacte containers waar mogelijk)

== Definition of Done

Je kan een docent vragen om te verbeteren als onderstaande voorwaarden voldaan zijn:

* [ ] voorbereiding werd goedgekeurd door docenten
* [ ] er zijn minstens die tiers in de applicatie
* [ ] minstens één van de tiers is horizontaal oneindig schaalbaar
* [ ] de repository is volledig. De yaml bestanden zijn voorzien van commentaar waar relevant
* [ ] gevoelige informatie zit allemaal in secrets

== Docker Compose steps

* Spring Boot
** generate simple app via springboot website to display Static HTML AND make use of MySQL connection
** Include Dockerfile to app-directory (FROM maven:3.6-jdk-8)

* Add NGINX app.conf file
** list port 80
** setup as proxy
** no self-created image needed, only app.config copy in docker-compose.yaml

* MySQL
** with Dockerfile to keep best practices in mind
*** Prevent running container as root (dangerous if malicious user escapes the container)
*** Prevent issues with persistent storage (host file system permissions) by creating and assigning a specific user with gid
** set password same as password in springboot-mysql-connection (app.properties)

[source, docker]
---
FROM mysql:8.0
RUN addgroup --gid 1024 app
RUN adduser app --disabled-password -gecos "" --gid 1024 
USER app
---


[source, bash]
---
# to fix permission issues with mounted volumes 
# create a GID on the host system, create a user with that guid in the container
# check all used guid's
getent group
# create the data-volume on host system
mkdir docker-data
# set group ownership of the directory to some GID
chown :1042 ./docker-data
# change permissions on the directory, security best practice
chmod 755 ./docker-data
# ensure all future content in the folder will inherit the group ownership
chmod g+s ./docker-data
---

[source, bash]
---
# optional add your host user to the group allowing easy access to the directory on the host machine
adduser serveradmin 1024
---

=== docker-compose.yaml

[source, yaml]
---
version: '3'
services:
  nginx:
   container_name: my-nginx
   image: nginx:1.15
   ports:
   - 80:80
   - 8443:443
   volumes:
   - ./nginx/conf.d:/etc/nginx/conf.d # copies the app.conf of the repo into the docker container
   depends_on:
   - app
 
  mysql: 
   container_name: my-mysql
   image: vincentbockaert/t11-kubs-mysql
   environment:
    MYSQL_DATABASE: t11db
    MYSQL_ROOT_PASSWORD: Hetuuris@123 # same as in the spring-boot app --> app.properties
    MYSQL_ROOT_HOST: '%'
   volumes:
    # Mount persistent volume on host system
    - ./docker-data:/var/lib/mysql
     # When a container is started for the first time, a new database with the specified name will be created and initialized with the provided configuration variables. 
    # Furthermore, it will execute files with extensions .sh, .sql and .sql.gz that are found in /docker-entrypoint-initdb.d
    # Creates the db
    - ./sql-scripts:/docker-entrypoint-initdb.d 
   ports:
   - "3306:3306"
  
  app:
    #restart: always
    build: ./app
    working_dir: /app
    volumes:
      - ./app:/app
      - ~/.m2:/root/.m2
    expose:
      - "8080"
    command: nohup mvn clean spring-boot:run # nohup makes certain that the terminal can be exited without killing the proces
    depends_on:
      - mysql
    deploy:
      replicas: 3
volumes:
  docker-data:
---





